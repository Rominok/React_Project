import React, { Component } from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import Header from './Header';
import Home from './Home';
import Page from './Page';
import Basket from './Basket';
import Footer from './Footer';

class App extends Component {

  render() {

    return (
      <div className="App">
        <Header/>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/film/:name'component={Page} />
          <Route exact path='/basket' component={Basket}/>
          <Route render ={ ()=>(
            <div>
              EROR 404
            </div>
          )}/>
        </Switch>
        <Footer/>
      </div>
    );
  }
}

export default App;
