import React, { Component } from 'react';

const Footer =()=>(
  <footer>
    <div>
      <h3>
          <img alt='call' src='http://www.quadrantregulatory.com/wp-content/uploads/2015/06/phone.png'/>
        +380 68 95 28 18
      </h3>
      <h2>  Щоденно, 9:00—22:00 Безкоштовно по Україні з будь-якого номеру</h2>
    </div>
    <div>
      <h3>
        <img alt='email' src='https://icon-icons.com/icons2/1233/PNG/256/1492718759-mail_83619.png'/>
        info@planetakino.ua
      </h3>
      <h2>
        Всі звернення, надіслані на цю пошту, потраплять
        до власників та керівництва Планети Кіно
      </h2>
    </div>
  </footer>
);
export default Footer;
