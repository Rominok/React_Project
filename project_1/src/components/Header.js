import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
class Header extends Component{
  render(){
    return(
      <header className='header'>
        <NavLink activeClassName="activeLink" exact to='/'>Головна</NavLink>
        <NavLink activeClassName="activeLink" to='/basket'>Кошик</NavLink>
      </header>
    )
  }
}

export default Header;
