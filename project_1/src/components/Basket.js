import React, { Component } from 'react';
import { connect } from 'react-redux';


const ItemsList = ({img, type, number, film})=>(
  <li className='ItemsList'>
    <h4>Фільм: {film}</h4>
    <img alt='image' src={img}/>
    <h4>Тип місця: {type}</h4>
    <h4>Номер: {number}</h4>
  </li>
);
class Basket extends Component {

  render() {
    let {dataRooms} = this.props;
    let item = dataRooms.data;
    return (
      <div className="Basket">
        {
          item.length !== 0 ?
          item.map((e,key)=>(
              <ItemsList key={key}
                film ={e.film}
                img ={e.img}
                type ={e.number}
                number ={e.number}
              />
          )):<div className='null'>Відсутні придбані квитки</div>
        }
      </div>
    );
  }
}
// - - - - - - - -  -
const MapStateToProps = (state, ownProps) => {
  return {
    data: state.dataBasket,
    dataRooms: state.Rooms
  }
}


const ConnectedBasket = connect(
  MapStateToProps
)(Basket)

export default ConnectedBasket;
